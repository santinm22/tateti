package com.example.tateti

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button
import android.widget.TextView
import android.widget.Toast


class MainActivity : AppCompatActivity(),OnClickListener {
    override fun onClick(v: View?) {
        botonSeleccionado(v as Button)
    }
    private var cell= mutableMapOf<Int,String>()
    private var isX=true
    private var winner:String=""
    private val totalCell=9
    private lateinit var txtResult: TextView
    private val x="X"
    private val o="O"
    private var botones= arrayOfNulls<Button>(totalCell)
    private val combinaciones:Array<IntArray> = arrayOf(
        intArrayOf(0,1,2),
        intArrayOf(3,4,5),
        intArrayOf(6,7,8),
        intArrayOf(0,3,6),
        intArrayOf(1,4,7),
        intArrayOf(2,5,8),
        intArrayOf(0,4,8),
        intArrayOf(2,4,6),
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        txtResult=findViewById(R.id.txtResultado)

        for (i in 1..totalCell) {
            var button = findViewById<Button>(resources.getIdentifier("boton$i", "id", packageName))
            button.setOnClickListener(this)
            botones[i-1]=button
        }
    }
    private fun botonSeleccionado(button: Button){
        var index = 0
        when(button.id){
            R.id.boton1->index=0
            R.id.boton2->index=1
            R.id.boton3->index=2
            R.id.boton4->index=3
            R.id.boton5->index=4
            R.id.boton6->index=5
            R.id.boton7->index=6
            R.id.boton8->index=7
            R.id.boton9->index=8
        }
        playGame(index,button)
        ganador()
        actualizar()
    }

    private fun ganador(){
        if (cell.isNotEmpty()){
            for (combinacion in combinaciones){
                var(a,b,c) = combinacion
                if (cell[a]!=null && cell[a]==cell[b]&& cell[a]==cell[c]){
                    this.winner=cell[a].toString()
                }
            }
        }
    }

    private fun actualizar(){
        when{
            winner.isNotEmpty() ->{
                txtResult.text=resources.getString(R.string.winner,winner)
                txtResult.setTextColor(Color.GREEN)
            }
            cell.size == totalCell->{
                txtResult.text="Empate"
            }
            else ->{
                txtResult.text=resources.getString(R.string.next_player,if(isX)x else o)
            }
        }
}

    private fun playGame(index:Int,button:Button){
        if (!winner.isNullOrEmpty()){
            Toast.makeText(this, "Juego Finalizado", Toast.LENGTH_SHORT).show()
            return
        }
        when{
            isX->cell[index]=x
            else->cell[index]=o
        }
        button.text=cell[index]
        button.isEnabled=false
        isX=!isX
    }

    fun resetButton(){
        for (i in 1..totalCell) {
        var button = botones[i-1]
        button?.text=""
        button?.isEnabled=true
        }
    }

    fun nuevoJuego(){
    cell= mutableMapOf()
    isX=true
    winner=""
    txtResult.text=resources.getString(R.string.next_player,x)
    resetButton()
    }
    fun reset(view:View){
    nuevoJuego()
    txtResult.setTextColor(Color.BLACK)
    }

}